import numpy as np
import yaml
import glob
import scipy.io as sio
import h5py


def unpack_configs(config):
    train_folder = config['train_folder']
    train_filenames = sorted(glob.glob(train_folder + '/*.mat'))
    val_folder = config['val_folder']
    val_filenames = sorted(glob.glob(val_folder + '/*.mat'))
    return train_filenames, val_filenames


def unpack_test_configs(config):
    test_folder = config['test_folder']
    language_folder = config['test_language_dir']
    test_filenames = sorted(glob.glob(test_folder + '/*.mat'))
#    language = sio.loadmat(language_folder)
#    language = language['train_language_feature']
    language_features = sorted(glob.glob(language_folder + '/*.mat'))

    return test_filenames, language_features

def adjust_learning_rate(config, epoch, step_idx, learning_rate):
    # Adapt Learning Rate
    if config['lr_policy'] == 'step':
        if epoch == config['lr_step'][step_idx]:
            learning_rate.set_value(
                np.float32(learning_rate.get_value() / 10))   #learning_rate is the shared variable
            step_idx += 1
            if step_idx >= len(config['lr_step']):
                step_idx = 0  # prevent index out of range error
            print 'Learning rate changed to:', learning_rate.get_value()

    return step_idx


def prepare_data(img, img_mean, img_dim=256, cropped_dim=224):


    target_shape = (3, cropped_dim, cropped_dim)
#    assert img.dtype == 'uint8', img_name
    # assert False
    if len(img.shape) == 2:  #gray-scale image
        img = np.resize(img, (img_dim, img_dim))
        img = np.asarray([img, img, img])
        img = np.rollaxis(img, 0, 3)  #3*256*256
    else:
        if img.shape[2] > 3: #special image
            img = img[:, :, :3]
        img = np.resize(img, (img_dim, img_dim, 3))

    img = img[:, :, ::-1] - img_mean
    img = np.swapaxes(img, 0, 1)   

    img = np.rollaxis(img, 2, 0)  # 3*256*256

    img = np.resize(img, target_shape)  
    img = img[np.newaxis, :, :, :]
       
    return img  




def test_model_wrap(test_model, test_labels, test_filenames, language_features):   

    test_features = []
    test_errors = []

    for index in xrange(len(test_filenames)):
        data = h5py.File(test_filenames[index])
        batch_feature = np.asarray(data['predicate_activation'])
        batch_feature = batch_feature[np.newaxis, :,:,:]

        batch_label = test_labels[index].astype('int32')
        language_feature = language_features[index,:].astype('float32')
        language_feature = np.reshape(language_feature, (1, 600))

        error = test_model(batch_feature, language_feature, batch_label)

        test_errors.append(error)

    this_test_error = np.mean(test_errors)

    return this_test_error
