import scipy.io as sio
import h5py
import sys
import time
import os
import yaml
import numpy as np
import glob
import math
import pickle
from random import shuffle
from utils import unpack_test_configs, adjust_learning_rate, test_model_wrap, prepare_data

def main(config):

    import theano.sandbox.cuda
    theano.sandbox.cuda.use(config['gpu'])
    import lasagne
    from model import compile_debug_model

    test_filenames, language= unpack_test_configs(config)
    (test_network, test_model) = compile_debug_model(config)

    img_mean = np.load(config['mean_file'])
#    img_mean = np.rollaxis(img_mean, 2,0).astype('float32')

    minibatch_range = range(len(test_filenames))
    print("Compilation complete, starting testing...")  

    load_epoch = 15

    model_file = './weights_l1/model_' + str(load_epoch) + '.npy'
    weights = np.load(model_file)

    lasagne.layers.set_all_param_values(test_network['attention'], weights[0:30])  

    save_dir = './plot/'


    for index in minibatch_range:


        data = h5py.File(test_filenames[index])
        data = np.asarray(data['predicate_activation'])
        data = data[np.newaxis,:,:,:]

        sub_language_feature = language[index,:].astype('float32')
        sub_language_feature = np.reshape(sub_language_feature, (1, 600))

        predict_feature = test_model(data, sub_language_feature)[0]
#        predict_feature = test_model(data)[0]

        predict_feature = {'predict_feature':np.squeeze(predict_feature)}

        sio.savemat(save_dir + '%05d' % (index) + '.mat', predict_feature)    


if __name__ == '__main__':

    with open('test_config.yaml', 'r') as f:
        config = yaml.load(f)
    main(config)
