import scipy.io as sio
import h5py
import sys
import time
import os
import yaml
import numpy as np
import glob
import math
import hickle as hkl
from random import shuffle
from utils import unpack_test_configs, adjust_learning_rate, test_model_wrap, prepare_data

def main(config):

    import theano.sandbox.cuda
    theano.sandbox.cuda.use(config['gpu'])
    import lasagne
    from model import compile_train_model, compile_test_model

    test_filenames, language_features = unpack_test_configs(config)
    (test_network, test_model) = compile_test_model(config)

    img_mean = np.load(config['mean_file'])
#    img_mean = np.rollaxis(img_mean, 2,0).astype('float32')

    minibatch_range = range(len(test_filenames))
    print("Compilation complete, starting testing...")  

    load_epoch = 15

    model_file = './weights/model_' + str(load_epoch) + '.npy'
    weights = np.load(model_file)
    lasagne.layers.set_all_param_values(test_network['prob'], weights)  

    save_dir = './relationships_prediction/'


    for index in minibatch_range:

        load_feature = h5py.File(test_filenames[index])
        element = load_feature['feature_container']
        language = sio.loadmat(language_features[index])
        language = language['vec_org']

        output = np.zeros((language.shape[0], 70))

        for jj in xrange(language.shape[0]):

            input_feature = load_feature[element[jj][0]][:]
            input_feature = input_feature[np.newaxis, :,:,:].astype('float32')

            sub_language_feature = language[jj,:].astype('float32')
            sub_language_feature = np.reshape(sub_language_feature, (1, 600))

            predict_feature = test_model(input_feature, sub_language_feature)[0]
            output[jj, :] = predict_feature

        predict_feature = {'predict_feature':output}

        sio.savemat(save_dir + '%05d' % (index) + '.mat', predict_feature)    


if __name__ == '__main__':

    with open('test_config.yaml', 'r') as f:
        config = yaml.load(f)
    main(config)
