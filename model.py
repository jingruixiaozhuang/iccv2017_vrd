import lasagne
from lasagne.layers import LocalResponseNormalization2DLayer
from lasagne.layers import InputLayer, ReshapeLayer
from lasagne.layers import DenseLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import Pool2DLayer as PoolLayer
from lasagne.layers import FeaturePoolLayer
from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.init import Normal
from lasagne.objectives import categorical_crossentropy
from lasagne.nonlinearities import softmax
from new_layers import model_layer, classifier_layer, attention_layer, MeanpoolLayer, transformed_layer
import numpy as np
import theano
import theano.tensor as T


def build_model(input_train_1, input_train_2, batch_size):

    net = {}
    net['input_1'] = InputLayer((batch_size, 3, 224 ,224), input_var=input_train_1)
    net['conv1_1'] = ConvLayer(
        net['input_1'], 64, 3, pad=1, flip_filters=False)
    net['conv1_2'] = ConvLayer(
        net['conv1_1'], 64, 3, pad=1, flip_filters=False)
    net['pool1'] = PoolLayer(net['conv1_2'], 2)
    net['conv2_1'] = ConvLayer(
        net['pool1'], 128, 3, pad=1, flip_filters=False)
    net['conv2_2'] = ConvLayer(
        net['conv2_1'], 128, 3, pad=1, flip_filters=False)
    net['pool2'] = PoolLayer(net['conv2_2'], 2)
    net['conv3_1'] = ConvLayer(
        net['pool2'], 256, 3, pad=1, flip_filters=False)
    net['conv3_2'] = ConvLayer(
        net['conv3_1'], 256, 3, pad=1, flip_filters=False)
    net['conv3_3'] = ConvLayer(
        net['conv3_2'], 256, 3, pad=1, flip_filters=False)
    net['pool3'] = PoolLayer(net['conv3_3'], 2)
    net['conv4_1'] = ConvLayer(
        net['pool3'], 512, 3, pad=1, flip_filters=False)
    net['conv4_2'] = ConvLayer(
        net['conv4_1'], 512, 3, pad=1, flip_filters=False)
    net['conv4_3'] = ConvLayer(
        net['conv4_2'], 512, 3, pad=1, flip_filters=False)
    net['pool4'] = PoolLayer(net['conv4_3'], 2)
    net['conv5_1'] = ConvLayer(
        net['pool4'], 512, 3, pad=1, flip_filters=False)
    net['conv5_2'] = ConvLayer(
        net['conv5_1'], 512, 3, pad=1, flip_filters=False)
    net['conv5_3'] = ConvLayer(
        net['conv5_2'], 512, 3, pad=1, flip_filters=False)

    net['input_2'] = InputLayer((batch_size, 600), input_var=input_train_2)
    net['transformed_language'] = DenseLayer(net['input_2'], 20)
      
    net['model_1'] = model_layer(net['transformed_language'], W1=lasagne.init.Normal(0.005), W2=lasagne.init.Normal(0.005), feature_dim=512, rank=20)
    net['model_2'] = model_layer(net['transformed_language'], W1=lasagne.init.Normal(0.005), W2=lasagne.init.Normal(0.005), feature_dim=512, rank=20)

    net['attention'] = attention_layer([net['conv5_3'], net['model_1']])
    net['fc'] = classifier_layer([net['attention'], net['model_2']])
    net['prob'] = NonlinearityLayer(net['fc'], softmax) 

    return net



def compile_train_model(config):


# build the training model
    train_batch_size = config['batch_size']   #number of bags
    input_train_1 = T.tensor4('input_train_1')
    input_train_2 = T.matrix('input_train_2')
    language_prior = T.matrix('language_prior')
    target_var = T.ivector('targets')
    train_network = build_model(input_train_1, input_train_2, train_batch_size)

    learning_rate = theano.shared(np.float32(config['learning_rate']))
    classification_scores = lasagne.layers.get_output(train_network['prob']) 
    model = lasagne.layers.get_output(train_network['model_2'])
    
    params = lasagne.layers.get_all_params(train_network['fc'], trainable=True)

    loss1 = T.mean(categorical_crossentropy(classification_scores, target_var)) 
    loss2 = 0.01*lasagne.regularization.l2(model)
    loss = loss1 + loss2

    grads = T.grad(loss, params)

    for index in range(26,len(grads)):
        grads[index] *= 10.0
        
    y_pred = T.argmax(classification_scores, axis=1)
    error = T.mean(T.neq(y_pred, target_var))
    updates = lasagne.updates.adam(grads, params, learning_rate)

    train_model = theano.function([input_train_1, input_train_2, target_var], [loss, error], updates=updates) 
 
    return train_network, train_model, learning_rate


def compile_test_model(config):
    test_1 = T.tensor4('input_test_1')
    test_2 = T.matrix('input_test_2')
    target_var = T.ivector('target_var')
    test_network = build_model(test_1, test_2, 1)
    prob = lasagne.layers.get_output(test_network['prob'], deterministic=True)
#    y_pred = T.argmax(prob, axis=1)
#    error = T.mean(T.neq(y_pred, target_var))
#    test_model = theano.function([test_1, test_2, target_var], [error])  # for testing, uncomment these lines
    test_model = theano.function([test_1, test_2], [prob])  # for extracting features after training
    return test_network, test_model




