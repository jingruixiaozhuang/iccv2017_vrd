import scipy.io as sio
import h5py
import sys
import time
import os
import yaml
import numpy as np
import glob
import math
import pickle
from random import shuffle
from utils import unpack_configs, adjust_learning_rate, test_model_wrap, prepare_data

def main(config):

    import theano.sandbox.cuda
    theano.sandbox.cuda.use(config['gpu'])
    import lasagne
    from model import compile_train_model, compile_test_model

    train_filenames, val_filenames = unpack_configs(config)
    (train_network, train_model, learning_rate) = compile_train_model(config)
    (test_network, test_model) = compile_test_model(config)
    train_labels = sio.loadmat(config['train_label_file'])
    train_labels = train_labels['train_predicate_labels']
    val_labels = sio.loadmat(config['val_label_file'])
    val_labels = val_labels['test_predicate_labels']

#   language feature
    language = sio.loadmat(config['train_language_feature'])
    language = language['train_language_feature']


    val_language = sio.loadmat(config['test_language_feature'])
    val_language = val_language['test_language_feature']


    index_shuf = range(len(train_labels))
    shuffle(index_shuf)

    train_filenames_shuf = []
    train_labels_shuf = []
    language_shuf = np.zeros((21066, 600))


    count = 0
    for i in index_shuf:
        train_filenames_shuf.append(train_filenames[i])
        train_labels_shuf.append(train_labels[i])
        language_shuf[count,:] = language[i,:]
        count = count + 1

    batch_size = config['batch_size']


    print("Compilation complete, starting training...")   
    n_train_batches = int(len(train_filenames) / batch_size)
    minibatch_range = range(n_train_batches)

    epoch = 0
    step_idx = 0
    test_record = []

    model_file = config['finetune_weights_dir'] 
#    weights = np.load(model_file)
    with open(model_file, 'rb') as f:
        weights = pickle.load(f)

    lasagne.layers.set_all_param_values(train_network['conv5_3'], weights['param values'][0:26])


    while epoch < config['n_epochs']:

        if config['shuffle']:
            np.random.shuffle(minibatch_range)

    	epoch = epoch + 1
        if config['resume_train'] and epoch == 1:
            load_epoch = config['load_epoch']
            model_file = './weights/model_' + str(load_epoch) + '.npy'
            weights = np.load(model_file)
            lasagne.layers.set_all_param_values(train_network['prob'], weights)
            epoch = load_epoch + 1
            lr_to_load = np.load(
                config['weights_dir'] + 'lr_' + str(load_epoch) + '.npy')
#            test_record = list(
#                np.load(config['weights_dir'] + 'test_record.npy'))
            learning_rate.set_value(lr_to_load)

        count = 0
        for minibatch_index in minibatch_range:

            num_iter = (epoch - 1) * n_train_batches + count
            count = count + 1
            if count == 1:
                s = time.time()
            if count == 20:
                e = time.time()
                print "time per 20 iter:", (e - s)

            batch_feature = np.zeros((batch_size, 3, 224, 224), 'float32')
            language_feature = language_shuf[minibatch_index*batch_size:(minibatch_index+1)*batch_size, :].astype('float32')

            for index in range(batch_size):
                data = h5py.File(train_filenames_shuf[minibatch_index*batch_size + index])
                data = np.asarray(data['predicate_activation'])
                data = data[np.newaxis,:,:,:]
                batch_feature[index, :, :, :] = data
            batch_label = np.squeeze(train_labels_shuf[minibatch_index*batch_size:(minibatch_index+1)*batch_size])
            batch_label = np.asarray(batch_label).astype('int32')
            cost, error = train_model(batch_feature, language_feature, batch_label)

          
            if num_iter % config['print_freq'] == 0:
                print 'training @ iter = ', num_iter
                print 'training cost:', cost
                if config['print_train_error']:
                    print 'training error rate:', error             


        if epoch % config['snapshot_freq'] == 0:
            model_file = config['weights_dir'] + 'model_' + str(epoch) + '.npy'
            values = lasagne.layers.get_all_param_values(train_network['prob'])
            np.save(config['weights_dir'] + 'lr_' + str(epoch) + '.npy',
                       learning_rate.get_value())
            np.save(model_file, values)

### transfer
        train_params = lasagne.layers.get_all_param_values(train_network['prob'])
        lasagne.layers.set_all_param_values(test_network['prob'], train_params)   

        this_test_error = test_model_wrap(test_model, val_labels, val_filenames, val_language)

        print('epoch %i: validation error %f %%' %
              (epoch, this_test_error * 100.))
        test_record.append([this_test_error])       
        np.save(config['weights_dir'] + 'test_record.npy', test_record)  
        step_idx = adjust_learning_rate(config, epoch, step_idx, learning_rate)



if __name__ == '__main__':

    with open('./config.yaml', 'r') as f:
        config = yaml.load(f)
        
    main(config)















