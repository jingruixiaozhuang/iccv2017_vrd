import lasagne
from lasagne.layers import Layer, MergeLayer
import numpy as np
import theano
import theano.tensor as T
import numpy as np


class model_layer(Layer):

    def __init__(self, incoming, W1, W2, feature_dim, rank, **kwargs):
        super(model_layer, self).__init__(incoming, **kwargs)
        self.W1 = self.add_param(W1, (feature_dim, 70), name = 'W1')
        self.W2 = self.add_param(W2, (rank, feature_dim, 70), name = 'W2')
        self.feature_dim = feature_dim
        self.rank = rank

    def get_output_for(self, input, **kwargs):

        reshape_input_1 = T.extra_ops.repeat(input.dimshuffle(0,1,'x'), self.feature_dim, axis=2)
        reshape_input_2 = T.extra_ops.repeat(reshape_input_1.dimshuffle(0,1,2,'x'), 70, axis=3)
        reshape_W2 = T.extra_ops.repeat(self.W2.dimshuffle('x',0,1,2), self.input_shape[0], axis=0)
        output = T.sum(reshape_input_2*reshape_W2, axis=1)
        classifier = T.extra_ops.repeat(self.W1.dimshuffle('x',0,1), self.input_shape[0], axis=0) + output
        return classifier

    def get_output_shape_for(self, input_shape):
    	return (input_shape[0], self.feature_dim, 70)



class classifier_layer(MergeLayer):

    def __init__(self, incomings, **kwargs):
        super(classifier_layer, self).__init__(incomings, **kwargs)
        b = lasagne.init.Normal(0.01)
        self.b = self.add_param(b, (1, 70), name='b')
    
    def get_output_for(self, inputs, **kwargs):

        output = inputs[0] * inputs[1]
        output_reshape = T.sum(output, axis=1) + self.b
        return output_reshape

    def get_output_shape_for(self, input_shapes):
        return (input_shapes[0][0], input_shapes[1][2])

   

class attention_layer(MergeLayer):
    # inputs[0] b x 512 x 14 x 14
    # inputs[1] b x 512 x 70        
    def get_output_for(self, inputs, **kwargs):

#    generate attention map
    
        tile_input = inputs[0].dimshuffle(0,1,2,3,'x')        # b x 512 x 14 x 14 x *
        tile_language = inputs[1].dimshuffle(0,1,'x','x',2)   # b x 512 x * x * x 70
        attention_map = T.sum(tile_input * tile_language, axis=1)  #b x 14 x 14 x 70
        
#      normalize the attention map
        attention_numerator= T.nnet.nnet.softplus(attention_map) + 0.1           # b x 14 x 14 x 70
        attention_denominator= T.sum(T.sum(attention_numerator,axis=2),axis=1)   # b x 70
        normalized_attention = attention_numerator/attention_denominator.dimshuffle(0,'x','x',1) # b x 14 x 14 x 70

#     get the output
        output = T.sum(T.sum(normalized_attention.dimshuffle(0,'x',1,2,3)*tile_input,axis=3),axis=2)

        return output

    def get_output_shape_for(self, input_shapes):
        return (input_shapes[0][0], input_shapes[0][1], 70)



class MeanpoolLayer(Layer):

    def get_output_for(self, input, **kwargs):
    	output = T.mean(T.mean(input, axis=3), axis=2)
    	return output

    def get_output_shape_for(self, input_shape):
    	return (input_shape[0], input_shape[1])



class transformed_layer(Layer):
    def __init__(self, incoming, rank, **kwargs):
    	super(transformed_layer, self).__init__(incoming, **kwargs)
    	W = lasagne.init.Normal(0.01)
    	self.rank = rank
    	self.W = self.add_param(W, (self.input_shape[1], self.rank), name='transformed_W')

    def get_output_for(self, input, **kwargs):
    	output = T.nnet.relu(T.dot(input, self.W))
    	return output

    def get_output_reshape_for(self, input_shape):
    	return (input_shape[0], self.rank)



